
public class SwitchingVariables {

	public static void main(String[] args) {
		String a = "water";
		String b = "cola";
		String temp;
		
		System.out.println("First a: "+a);
		System.out.println("First b: "+b);
		
		temp=a;
		a=b;
		b=temp;
		
		System.out.println("Second a: "+a);
		System.out.println("Second b: "+b);
		
		System.out.println("\n \n");
		
		String C = "wasser";
		String D = "Nutte";
		String tempo;
		
		System.out.println("First C:"+C);
		System.out.println("First D:"+D);
		
		tempo=C;
		C=D;
		D=tempo;
		
		System.out.println("Second C:"+C);
		System.out.println("Second D:"+D);
		
	}

}
