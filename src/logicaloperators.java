import java.util.Scanner;

public class logicaloperators {

	public static void main(String[] args) {
		// logical operators = used to connect wo or more expressions
		//
		//					&& = (AND) both conditions must be true
		//					|| = (OR) either condition is true
		//					!  = (NOT) reverses boolean value of condition
		
		int temp = 19;
		
		if(temp>30) {
			System.out.println("Its hot outside");
		}
		else if(temp>=20 && temp<=30) {
			System.out.println("Its kinda hot outside");
		}
		else {
			System.out.println("Its cold outside");
		}
		
					Scanner scanner = new Scanner(System.in);
		
					System.out.println("You are playing a Game , type q or Q to exit the game");
					String response = scanner.next();
		
					//OR method
					if(response.equals("q") || response.equals("Q")) {
						System.out.println("You quit the game!");
					}
					else {
						System.out.println("You are still playing the game!");
					}
					
					Scanner scanner1 = new Scanner(System.in);
					
					System.out.println("You are playing a Game , type q or Q to exit the game");
					String response1 = scanner1.next();
		
					//NOT method
					if(!response1.equals("q") && !response1.equals("Q")) {
						System.out.println("You are still playing the game!");
					}
					else {
						System.out.println("You quit the game!");
					}
					scanner1.close();
					scanner.close();

		
		
		
		
		
	}

}
